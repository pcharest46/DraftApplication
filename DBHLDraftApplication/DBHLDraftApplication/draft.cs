﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBHLDraftApplication
{
    public class draft
    {
        private List<joueur> ListeJoueursDispo;
        private List<ronde> ListeRondes;
        private string draft_ID;

        public draft()
        {
            this.draft_ID = "1";
            this.ListeJoueursDispo = new List<DBHLDraftApplication.joueur>();
            this.ListeRondes = new List<DBHLDraftApplication.ronde>();
        }
        public draft(string draft_ID)
        {
            this.draft_ID = draft_ID;
            this.ListeJoueursDispo = new List<DBHLDraftApplication.joueur>();
            this.ListeRondes = new List<DBHLDraftApplication.ronde>();
        }
        public void AddRoundToDraft(ronde temp)
        {
            ListeRondes.Add(temp);
        }
        public void AddPlayerToDraftList(joueur temp)
        {
            ListeJoueursDispo.Add(temp);
        }
        public void RemoveSelectedPlayer(joueur temp)
        {
            ListeJoueursDispo.Remove(temp);
            ListeJoueursDispo.TrimExcess();
        }
        public List<joueur> GetPlayerList()
        {
            return ListeJoueursDispo;
        }
        public List<ronde> GetRoundList()
        {
            return ListeRondes;
        }

    }
}