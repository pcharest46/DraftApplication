﻿namespace DBHLDraftApplication
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.dgvPlayersAvailable = new System.Windows.Forms.DataGridView();
            this.btnDraft = new System.Windows.Forms.Button();
            this.ListTeams = new System.Windows.Forms.ListBox();
            this.ListTeamPlayers = new System.Windows.Forms.ListBox();
            this.pBox1 = new System.Windows.Forms.PictureBox();
            this.pBox5 = new System.Windows.Forms.PictureBox();
            this.pBox4 = new System.Windows.Forms.PictureBox();
            this.pBox3 = new System.Windows.Forms.PictureBox();
            this.pBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pBoxSelection = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tmrpick = new System.Windows.Forms.Timer(this.components);
            this.lblConnect = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlayersAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxSelection)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPlayersAvailable
            // 
            this.dgvPlayersAvailable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPlayersAvailable.Location = new System.Drawing.Point(13, 191);
            this.dgvPlayersAvailable.MultiSelect = false;
            this.dgvPlayersAvailable.Name = "dgvPlayersAvailable";
            this.dgvPlayersAvailable.RowTemplate.Height = 24;
            this.dgvPlayersAvailable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPlayersAvailable.Size = new System.Drawing.Size(1074, 559);
            this.dgvPlayersAvailable.TabIndex = 0;
            this.dgvPlayersAvailable.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPlayersAvailable_CellDoubleClick);
            this.dgvPlayersAvailable.SelectionChanged += new System.EventHandler(this.dgvPlayersAvailable_SelectionChanged);
            // 
            // btnDraft
            // 
            this.btnDraft.Location = new System.Drawing.Point(1090, 796);
            this.btnDraft.Name = "btnDraft";
            this.btnDraft.Size = new System.Drawing.Size(183, 64);
            this.btnDraft.TabIndex = 1;
            this.btnDraft.Text = "Draft";
            this.btnDraft.UseVisualStyleBackColor = true;
            this.btnDraft.Click += new System.EventHandler(this.btnDraft_Click);
            // 
            // ListTeams
            // 
            this.ListTeams.FormattingEnabled = true;
            this.ListTeams.ItemHeight = 16;
            this.ListTeams.Location = new System.Drawing.Point(1097, 22);
            this.ListTeams.Name = "ListTeams";
            this.ListTeams.Size = new System.Drawing.Size(203, 212);
            this.ListTeams.TabIndex = 2;
            this.ListTeams.SelectedIndexChanged += new System.EventHandler(this.ListTeams_SelectedIndexChanged);
            // 
            // ListTeamPlayers
            // 
            this.ListTeamPlayers.FormattingEnabled = true;
            this.ListTeamPlayers.ItemHeight = 16;
            this.ListTeamPlayers.Location = new System.Drawing.Point(1306, 22);
            this.ListTeamPlayers.Name = "ListTeamPlayers";
            this.ListTeamPlayers.Size = new System.Drawing.Size(199, 212);
            this.ListTeamPlayers.TabIndex = 3;
            // 
            // pBox1
            // 
            this.pBox1.Location = new System.Drawing.Point(18, 17);
            this.pBox1.Name = "pBox1";
            this.pBox1.Size = new System.Drawing.Size(135, 120);
            this.pBox1.TabIndex = 4;
            this.pBox1.TabStop = false;
            // 
            // pBox5
            // 
            this.pBox5.Location = new System.Drawing.Point(673, 17);
            this.pBox5.Name = "pBox5";
            this.pBox5.Size = new System.Drawing.Size(135, 120);
            this.pBox5.TabIndex = 5;
            this.pBox5.TabStop = false;
            // 
            // pBox4
            // 
            this.pBox4.Location = new System.Drawing.Point(515, 17);
            this.pBox4.Name = "pBox4";
            this.pBox4.Size = new System.Drawing.Size(135, 120);
            this.pBox4.TabIndex = 6;
            this.pBox4.TabStop = false;
            // 
            // pBox3
            // 
            this.pBox3.Location = new System.Drawing.Point(351, 17);
            this.pBox3.Name = "pBox3";
            this.pBox3.Size = new System.Drawing.Size(135, 120);
            this.pBox3.TabIndex = 7;
            this.pBox3.TabStop = false;
            // 
            // pBox2
            // 
            this.pBox2.Location = new System.Drawing.Point(184, 17);
            this.pBox2.Name = "pBox2";
            this.pBox2.Size = new System.Drawing.Size(135, 120);
            this.pBox2.TabIndex = 8;
            this.pBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(348, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(512, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(670, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1115, 733);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "label6";
            // 
            // pBoxSelection
            // 
            this.pBoxSelection.Location = new System.Drawing.Point(1118, 595);
            this.pBoxSelection.Name = "pBoxSelection";
            this.pBoxSelection.Size = new System.Drawing.Size(135, 120);
            this.pBoxSelection.TabIndex = 14;
            this.pBoxSelection.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 843);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "label7";
            // 
            // tmrpick
            // 
            this.tmrpick.Interval = 1000;
            this.tmrpick.Tick += new System.EventHandler(this.tmrpick_Tick);
            // 
            // lblConnect
            // 
            this.lblConnect.AutoSize = true;
            this.lblConnect.Location = new System.Drawing.Point(890, 155);
            this.lblConnect.Name = "lblConnect";
            this.lblConnect.Size = new System.Drawing.Size(74, 17);
            this.lblConnect.TabIndex = 17;
            this.lblConnect.Text = "lblConnect";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1094, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "TEAMS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1306, 249);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "PLAYERS";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1914, 1045);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblConnect);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pBoxSelection);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pBox2);
            this.Controls.Add(this.pBox3);
            this.Controls.Add(this.pBox4);
            this.Controls.Add(this.pBox5);
            this.Controls.Add(this.pBox1);
            this.Controls.Add(this.ListTeamPlayers);
            this.Controls.Add(this.ListTeams);
            this.Controls.Add(this.btnDraft);
            this.Controls.Add(this.dgvPlayersAvailable);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "DBHL Draft Application";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlayersAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxSelection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPlayersAvailable;
        private System.Windows.Forms.Button btnDraft;
        private System.Windows.Forms.ListBox ListTeams;
        private System.Windows.Forms.ListBox ListTeamPlayers;
        private System.Windows.Forms.PictureBox pBox1;
        private System.Windows.Forms.PictureBox pBox5;
        private System.Windows.Forms.PictureBox pBox4;
        private System.Windows.Forms.PictureBox pBox3;
        private System.Windows.Forms.PictureBox pBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pBoxSelection;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer tmrpick;
        private System.Windows.Forms.Label lblConnect;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}