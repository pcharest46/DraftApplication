﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using MySql.Data.MySqlClient;

namespace DBHLDraftApplication
{
    public partial class Form2 : Form
    {
        public draft dbhldraft=new draft("0");
        public ligue dbhl = new ligue();
        public static int round = 0;
        public static int selection = 0;
        public static int nbteam = 0;
        public List<equipe> ListeOrdreEquipeDraft = new List<equipe>();
        public List<capitaine> ListeCapitaine = new List<capitaine>();

        int m = 2;
        int sec = 0;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
            Form1 frm1 = new Form1();
            frm1.Hide();
            this.Show();
            label7.Text = m.ToString() + ":0" + sec.ToString();
            string path="";
            OpenFileDialog OpenFile1 = new OpenFileDialog();
            string Filename="aaa";

            if(OpenFile1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            {
                Filename = OpenFile1.FileName;
                path = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Filename + ";Extended Properties=\"Excel 8.0;HDR=Yes;\";";
                
            }

            OleDbConnection conn = new OleDbConnection(path);

            if(Filename!="")
            {
               
                OleDbDataAdapter myDataAdapter = new OleDbDataAdapter("Select * from [Allplayers$]", conn);
                DataTable dt = new DataTable();
                myDataAdapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    string identifier = row[0].ToString();
                    string prenom = row[1].ToString();
                    string nom = row[3].ToString();
                    string position = row[6].ToString();
                    bool capitano = Convert.ToBoolean(row[7]);
                    string photo = row[3].ToString();

                    // When it is a captain, he will be added and so will a team. Element will also be added to the list.
                    if(capitano==true)
                    {
                        int noround = Convert.ToInt32(row[8]);
                        capitaine captn = new capitaine("cap00"+(nbteam+1).ToString(),noround,identifier,position,capitano,photo,prenom,nom);
                        ListeCapitaine.Add(captn);
                        nbteam++;
                        equipe team = new DBHLDraftApplication.equipe("e00" + (nbteam + 1).ToString(), "Team" + (nbteam + 1).ToString(), "Team" + (nbteam + 1).ToString() + ".jpeg", captn);
                        dbhl.AddTeamToLeague(team);
                        //Remplir et Initialiser la liste item TeamList
                        ListTeams.Items.Add(team.ID +" team " + team.CAPITAINE.NOM);
                        ListTeams.SelectedIndex = 0;
                    }
                    else
                    {
                        joueur player = new joueur(identifier, position, capitano, photo, prenom, nom);
                        dbhldraft.AddPlayerToDraftList(player);
                    }
                    conn.Close();
                }
                

                //Assigner la liste de joueur au DGV et Modif non permises
                dgvPlayersAvailable.DataSource = dbhldraft.GetPlayerList();
                dgvPlayersAvailable.ReadOnly = true;

                //Preparer les rondes pour le draft
                for (int i = 1; i <= DBHLDraftApplication.Form1.nbjoueurs; i++)
                {
                    ronde Round = new ronde(i);
                    dbhldraft.AddRoundToDraft(Round);

                }
                RandomizeOrdreDraftTeam();
                //Assigner photos a PictureBox
                PicBoxAssignment(selection);
                tmrpick.Enabled = true;
                tmrpick.Start();
            }
            else
            {
                MessageBox.Show("Connection Error!");
            }
            
        }

        

        private void tmrpick_Tick(object sender, EventArgs e)
        {
            IncrementRoundOrSelection();
            string visual = "00";

            // At the start of every selection (2min), auto-draft the captain if it is his round_worth and also his team selecting.
            if (m == 2 && sec == 0)
            {
                //Pick the captain if he is first overall selection (thus in Form Load)
                autoDraftPlayerCaptain(ListeOrdreEquipeDraft[selection].CAPITAINE);
                tmrpick.Stop();
                tmrpick.Start();
            }

            sec -= 1;

            if (m == 0 && sec == 0)
            {
                tmrpick.Stop();
                label7.Text = m.ToString() + ":" + visual;
                MessageBox.Show("Timer up!");
                int rowIndex = dgvPlayersAvailable.CurrentCell.RowIndex;
                PickPlayer(rowIndex);
                //Assigner photos a PictureBox
                PicBoxAssignment(selection);
            }
            if (sec==0)
            {
                label7.Text = m.ToString() + ":" + visual;
            }
            else
            {
                if (sec == -1)
                {
                    m -= 1;
                    sec = 59;
                    label7.Text = m.ToString() + ":" + sec.ToString();
                }
                else if (sec > 0 && sec < 10)
                {
                    label7.Text = m.ToString() + ":0" + sec.ToString();
                }
                else
                {
                    label7.Text = m.ToString() + ":" + sec.ToString();
                }
            }
        }
        
        private void dgvPlayersAvailable_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            PickPlayer(rowIndex);
            //Assigner photos a PictureBox
            PicBoxAssignment(selection);
        }

        private void btnDraft_Click(object sender, EventArgs e)
        {
            int rowIndex=dgvPlayersAvailable.CurrentCell.RowIndex;
            PickPlayer(rowIndex);
            //Assigner photos a PictureBox
            PicBoxAssignment(selection);
        }

        //  Rentrer le row dans le DataGridView et Sortir le joueur en question.

        private joueur IdentifyListElement(DataGridViewRow Selrow)
        {
            joueur temp = new joueur();

            for(int i=0;i<dbhldraft.GetPlayerList().Count;i++)
            {
                string identifier = Selrow.Cells[0].Value.ToString();
                if (identifier == dbhldraft.GetPlayerList()[i].ID)
                {
                    
                    temp=dbhldraft.GetPlayerList()[i];
                    break;
                }
            }
                return temp;
        }

        private equipe IdentifyListItem(string SelItemTeam)
        {
            equipe temp = new equipe();

            for (int i = 0; i < dbhl.GetLeagueTeamsList().Count; i++)
            {
                SelItemTeam = ListTeams.SelectedItem.ToString();
                if (SelItemTeam == dbhl.GetLeagueTeamsList()[i].ID + " team " + dbhl.GetLeagueTeamsList()[i].CAPITAINE.NOM)
                {
                    temp = dbhl.GetLeagueTeamsList()[i];
                    break;
                }
            }
            return temp;
        }
        

        private void RandomizeOrdreDraftTeam()
        {
            Random rand = new Random();
            List<int> tabIndex = new List<int>();

            //Randomize ordre des equipes qui draft et ensuite y aller en ordre Inverser (Snake Pick for 2 Rounds)

            //1er sens
            for (int i = 0; i < dbhl.GetLeagueTeamsList().Count; ++i)
            {
                tabIndex.Add(i);
            }

                for (int i = 0; i < dbhl.GetLeagueTeamsList().Count; ++i)
            {
                int index = rand.Next(0, tabIndex.Count);
                ListeOrdreEquipeDraft.Add(dbhl.GetLeagueTeamsList()[tabIndex[index]]);
                tabIndex.Remove(tabIndex[index]);
                tabIndex.TrimExcess();
            }
                //Sens Inverse
            for (int i = dbhl.GetLeagueTeamsList().Count-1; i >= 0; i--)
            {
                ListeOrdreEquipeDraft.Add(ListeOrdreEquipeDraft[i]);
            }

        }

        private void PickPlayer(int rowIndex)
        {
            IncrementRoundOrSelection();
            joueur player;
            DataGridViewRow row = dgvPlayersAvailable.Rows[rowIndex];
            player = IdentifyListElement(row);
            
            //Confirmation of draft pick
            DialogResult dr = MessageBox.Show("You are about to draft " + player.PRENOM + " " + player.NOM + ", are you sure?", "Draft Pick", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (dr == DialogResult.OK)
            {
                dbhldraft.RemoveSelectedPlayer(player);
                ListeOrdreEquipeDraft[selection].AddPlayerToTeam(player);
                dbhldraft.GetRoundList()[round].AddPlayerToRoundList(player);

                //add to Item List Team Players
                ListTeamPlayers.Items.Clear();
                equipe SelTeam;
                string ItemTeamSelected;
                ItemTeamSelected = ListTeams.SelectedItem.ToString();
                SelTeam = IdentifyListItem(ItemTeamSelected);
                for (int i = 0; i < SelTeam.GetTeamPlayers().Count(); i++)
                {
                    ListTeamPlayers.Items.Add(SelTeam.GetTeamPlayers()[i].PRENOM + " " + SelTeam.GetTeamPlayers()[i].NOM);
                }

                //Reset DGV data
                dgvPlayersAvailable.DataSource = null;
                dgvPlayersAvailable.DataSource = dbhldraft.GetPlayerList();
                //Reset timer for draft pick
                tmrpick.Stop();
                m = 2;
                sec = 0;
                label7.Text = m.ToString() + ":0" + sec.ToString();
                selection++;
                IncrementRoundOrSelection();
                tmrpick.Start();
            }
        }

        private void PicBoxAssignment(int teamIndex)
        {
            int i = 0;
            if (teamIndex + i == 18)
            {
                teamIndex = 0;
                i = 0;
            }
            pBox1.Image = Image.FromFile(Application.StartupPath + @"\images\" + ListeOrdreEquipeDraft[teamIndex+i].CAPITAINE.NOM+".jpg");
            pBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            label1.Text = ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.PRENOM + " " + ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.NOM;
            i++;
            if (teamIndex + i == 18)
            {
                teamIndex = 0;
                i = 0;
            }
            pBox2.Image = Image.FromFile(Application.StartupPath + @"\images\" + ListeOrdreEquipeDraft[teamIndex+i].CAPITAINE.NOM + ".jpg");
            pBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            label2.Text = ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.PRENOM + " " + ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.NOM;
            i++;
            if (teamIndex + i == 18)
            {
                teamIndex = 0;
                i = 0;
            }
            pBox3.Image = Image.FromFile(Application.StartupPath + @"\images\" + ListeOrdreEquipeDraft[teamIndex+i].CAPITAINE.NOM + ".jpg");
            pBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            label3.Text = ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.PRENOM + " " + ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.NOM;
            i++;
            if (teamIndex + i == 18)
            {
                teamIndex = 0;
                i = 0;
            }
            pBox4.Image = Image.FromFile(Application.StartupPath + @"\images\" + ListeOrdreEquipeDraft[teamIndex+i].CAPITAINE.NOM + ".jpg");
            pBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            label4.Text = ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.PRENOM + " " + ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.NOM;
            i++;
            if (teamIndex + i == 18)
            {
                teamIndex = 0;
                i = 0;
            }
            pBox5.Image = Image.FromFile(Application.StartupPath + @"\images\" + ListeOrdreEquipeDraft[teamIndex+i].CAPITAINE.NOM + ".jpg");
            pBox5.SizeMode = PictureBoxSizeMode.StretchImage;
            label5.Text = ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.PRENOM + " " + ListeOrdreEquipeDraft[teamIndex + i].CAPITAINE.NOM;
            i++;
            if (teamIndex + i == 18)
            {
                teamIndex = 0;
                i = 0;
            }
            
        }

        private void dgvPlayersAvailable_SelectionChanged(object sender, EventArgs e)
        {
            int rowIndex = dgvPlayersAvailable.CurrentCell.RowIndex;
            joueur player;
            DataGridViewRow row = dgvPlayersAvailable.Rows[rowIndex];
            player = IdentifyListElement(row);

            string path= Application.StartupPath + @"\images\unknown.jpg";

            if (!File.Exists(Application.StartupPath + @"\images\" + player.NOM + ".jpg"))
            {
                pBoxSelection.Image = Image.FromFile(Application.StartupPath + @"\\images\\unknown.jpg");
                pBoxSelection.SizeMode = PictureBoxSizeMode.StretchImage;
                label6.Text = player.PRENOM + " " + player.NOM;

            }
            else
            {
                pBoxSelection.Image = Image.FromFile(Application.StartupPath + @"\images\" + player.NOM + ".jpg");
                pBoxSelection.SizeMode = PictureBoxSizeMode.StretchImage;
                label6.Text = player.PRENOM + " " + player.NOM;
            }
            
        }

        private void ListTeams_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListTeamPlayers.Items.Clear();
            equipe SelTeam;
            string ItemTeamSelected;
            ItemTeamSelected = ListTeams.SelectedItem.ToString();
            SelTeam = IdentifyListItem(ItemTeamSelected);
            for(int i=0;i< SelTeam.GetTeamPlayers().Count();i++)
            {
                ListTeamPlayers.Items.Add(SelTeam.GetTeamPlayers()[i].PRENOM + " " + SelTeam.GetTeamPlayers()[i].NOM);
            } 
        }
        private void autoDraftPlayerCaptain(capitaine temp)
        {
            IncrementRoundOrSelection();


            if ((round+1) == ListeOrdreEquipeDraft[selection].CAPITAINE.RONDE_WORTH && ListeOrdreEquipeDraft[selection].CAPITAINE==temp)
                {
                for(int i=0;i<ListeCapitaine.Count();i++)
                {
                    if(ListeCapitaine[i].ID==temp.ID)
                    {
                        joueur player = new joueur(temp.ID, temp.POSITION, true, temp.PHOTO, temp.PRENOM, temp.NOM);
                        ListeOrdreEquipeDraft[selection].AddPlayerToTeam(player);
                        dbhldraft.GetRoundList()[round].AddPlayerToRoundList(player);
                        ListeCapitaine.Remove(temp);
                        ListeCapitaine.TrimExcess();
                        selection++;
                        MessageBox.Show("Auto-Draft: Captain of your team");
                        //Assigner photos a PictureBox
                        PicBoxAssignment(selection);
                    }
                }
                
                
            }
            IncrementRoundOrSelection();
        }

        private void IncrementRoundOrSelection()
        {
            EndOfDraft();

            //Changer de ronde une fois celle-ci rempli.
            if (dbhldraft.GetRoundList()[round].GetPlayerListRonde().Count == nbteam)
            {

                round++;
            }
            //Reset selection a 0 lorsqu'on a fait 18 selections "1 round" (snake restarts)
            if (selection == (nbteam * 2))
            {
                selection = 0;
            }
        }

        private void EndOfDraft()
        {
            Form1 frm1 = new DBHLDraftApplication.Form1();
            int nbequipesrempli = 0;
            for (int i=0;i<dbhl.GetLeagueTeamsList().Count;i++)
            {
                if(dbhl.GetLeagueTeamsList()[i].GetTeamPlayers().Count == DBHLDraftApplication.Form1.nbjoueurs)
                {
                    nbequipesrempli++;
                }
            }
            if(nbequipesrempli== nbteam)
            {
                ExcelWriteNewDocument();
                MessageBox.Show("The draft has ended! Best of luck to your team.");
               
                //Close Application
                Application.Exit();
            }
        }

        private void ExcelWriteNewDocument()
        {
            string path2 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=teams.xls;Extended Properties=\"Excel 8.0;HDR=Yes;\";";
            OleDbConnection conn2 = new OleDbConnection(path2);
            //conn2.Open();
            //OleDbDataAdapter myDataAdapter2 = new OleDbDataAdapter("Select * from [Feuil1]", conn2);
            //DataTable dt = new DataTable();
            //myDataAdapter2.Fill(dt);

                try
                {
                    conn2.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = conn2;
                    cmd.CommandText = @"Insert into [Feuil1$] (month,mango,apple,orange) 
            VALUES ('DEC','40','60','80');";
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //exception here
                }
                finally
                {
                    conn2.Close();
                    conn2.Dispose();
                }
            }

            //string nom;

            //for (int i = 0; i < nbteam; i++)
            //{
            //    for (int j = 0; j < DBHLDraftApplication.Form1.nbjoueurs; j++)
            //    {
            //        nom = dbhl.GetLeagueTeamsList()[i].GetTeamPlayers()[j].NOM;
                    
            //        OleDbCommand cmd = new OleDbCommand();
            //        cmd.Connection = conn2;
            //        cmd.CommandText = @"Insert into [Sheet1] (month,mango,apple,orange) VALUES ('DEC','40','60','80');";
            //        cmd.ExecuteNonQuery();
            //        conn2.Close();
            //    }
            //}
    }
}
