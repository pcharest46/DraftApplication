﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBHLDraftApplication
{
    public class ligue
    {
        private string ligue_ID;
        private string ligue_nom;
        private List<equipe> ListeEquipes;

        public ligue()
        {
            this.ligue_ID = "DBHL";
            this.ligue_nom = "DBHL";
            this.ListeEquipes = new List<equipe>();
        }
        
        public void AddTeamToLeague(equipe temp)
        {
            ListeEquipes.Add(temp);
        }
        public List<equipe> GetLeagueTeamsList()
        {
            return ListeEquipes;
        }
    }
}