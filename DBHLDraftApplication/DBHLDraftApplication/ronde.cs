﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBHLDraftApplication
{
    public class ronde
    {
        private int ronde_no;
        private List<joueur> ListeJoueursSelect;

        public ronde()
        {
            this.ronde_no = 0;
            this.ListeJoueursSelect = new List<DBHLDraftApplication.joueur>();
        }

        public ronde(int ronde_no)
        {
            this.ronde_no = ronde_no;
            this.ListeJoueursSelect = new List<DBHLDraftApplication.joueur>();
        }

        public List<joueur> GetPlayerListRonde()
        {
            return ListeJoueursSelect;
        }
        public void AddPlayerToRoundList(joueur temp)
        {
            ListeJoueursSelect.Add(temp);
        }
    }
}