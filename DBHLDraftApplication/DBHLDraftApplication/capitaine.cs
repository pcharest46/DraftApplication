﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBHLDraftApplication
{
    public class capitaine : joueur
    {
        private string capitaine_ID;
        private int round;

        public capitaine() : base()
        {
            this.capitaine_ID = "cap000";
            this.round = 0;
            this.prenom = "aaa";
            this.nom = "aaa";
        }

        public capitaine(string capitaine_ID, int round, string joueur_ID, string joueur_position,
            bool capitaine_YoN, string joueur_photo, string prenom, string nom) : base(joueur_ID, joueur_position,
            capitaine_YoN, joueur_photo, prenom, nom)
            
        {
            this.capitaine_ID = capitaine_ID;
            this.prenom = prenom;
            this.nom = nom;
            this.round = round;
        }
        public string CAP_ID
        {
            get
            {
                return capitaine_ID;
            }
            set { this.capitaine_ID = value; }
        }
        public string CAPT_PRENOM
        {
            get
            {
                return prenom;
            }
            set
            {
                prenom.ToLower();
                prenom = value;
            }
        }
        public string CAPT_NOM
        {
            get
            {
                return nom;
            }
            set
            {
                nom.ToLower();
                nom = value;
            }
        }
        public int RONDE_WORTH
        {
            get
            {
                return round;
            }
            set
            {
                round = value;
            }
        }
    }
}