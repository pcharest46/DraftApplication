﻿namespace DBHLDraftApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNomLigue = new System.Windows.Forms.Label();
            this.btnStartDraft = new System.Windows.Forms.Button();
            this.tBoxNbJoueurs = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNomLigue
            // 
            this.lblNomLigue.AutoSize = true;
            this.lblNomLigue.Location = new System.Drawing.Point(148, 18);
            this.lblNomLigue.Name = "lblNomLigue";
            this.lblNomLigue.Size = new System.Drawing.Size(46, 17);
            this.lblNomLigue.TabIndex = 0;
            this.lblNomLigue.Text = "label1";
            // 
            // btnStartDraft
            // 
            this.btnStartDraft.Location = new System.Drawing.Point(176, 126);
            this.btnStartDraft.Name = "btnStartDraft";
            this.btnStartDraft.Size = new System.Drawing.Size(141, 23);
            this.btnStartDraft.TabIndex = 31;
            this.btnStartDraft.Text = "Start Draft";
            this.btnStartDraft.UseVisualStyleBackColor = true;
            this.btnStartDraft.Click += new System.EventHandler(this.btnStartDraft_Click);
            // 
            // tBoxNbJoueurs
            // 
            this.tBoxNbJoueurs.Location = new System.Drawing.Point(224, 59);
            this.tBoxNbJoueurs.Name = "tBoxNbJoueurs";
            this.tBoxNbJoueurs.Size = new System.Drawing.Size(100, 22);
            this.tBoxNbJoueurs.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 17);
            this.label1.TabIndex = 33;
            this.label1.Text = "Number Players / Team";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 192);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tBoxNbJoueurs);
            this.Controls.Add(this.btnStartDraft);
            this.Controls.Add(this.lblNomLigue);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomLigue;
        private System.Windows.Forms.Button btnStartDraft;
        private System.Windows.Forms.TextBox tBoxNbJoueurs;
        private System.Windows.Forms.Label label1;
    }
}

