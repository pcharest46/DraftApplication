﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBHLDraftApplication
{
    public class joueur : personne
    {
        private string joueur_ID;
        private string joueur_position;
        private bool capitaine_YoN;
        private string joueur_photo;

        public joueur() : base()
        {
            this.joueur_ID = "j0";
            this.joueur_position = "000";
            this.capitaine_YoN = false;
            this.joueur_photo = "joueur0.jpg";

        }

        public joueur(string joueur_ID, string joueur_position,
            bool capitaine_YoN, string joueur_photo, string prenom, string nom) : base(prenom, nom)
        {
            this.joueur_ID = joueur_ID;

            this.joueur_position = joueur_position;
            this.capitaine_YoN = capitaine_YoN;
            this.joueur_photo = joueur_photo;
            this.prenom = prenom;
            this.nom = nom;
        }

        public string ID
        {
            get
            {
                return this.joueur_ID;
            }
            set { this.joueur_ID = value; }
        }

        public bool CAPITAINE
        {
            get
            {
                return capitaine_YoN;
            }
            set { this.capitaine_YoN = value; }
        }

        public string PHOTO
        {
            get
            {
                return joueur_photo;
            }
            set { this.joueur_photo = value; }
        }

        public string POSITION
        {
            get
            {
                return joueur_position;
            }
            set { this.joueur_position = value; }
        }
        public string PRENOM
        {
            get
            {
                return prenom;
            }
            set { this.prenom = value; }
        }
        public string NOM
        {
            get
            {
                return nom;
            }
            set { this.nom = value; }
        }
    }
}