﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBHLDraftApplication
{
    public class equipe
    {
        private string equipe_ID;
        private string equipe_nom;
        private string equipe_photo;
        private List<joueur> ListeJoueurs;
        private capitaine captain;
        public static int cnt=0;

        public equipe()
        {
            this.equipe_ID = "e000";
            this.equipe_nom = "e000";
            this.equipe_photo = "e000.jpg";
            this.ListeJoueurs = new List<joueur>();
            this.captain = new capitaine();
            cnt++;
        }

        public equipe(string equipe_ID,string equipe_nom, string equipe_photo, capitaine captain)
        {
            this.equipe_ID = equipe_ID;
            this.equipe_nom = equipe_nom;
            this.equipe_photo = equipe_photo;
            this.ListeJoueurs = new List<joueur>();
            this.captain = captain;
        }

        public List<joueur> GetTeamPlayers()
        {
            return ListeJoueurs;
        }
        public void AddPlayerToTeam(joueur temp)
        {
            ListeJoueurs.Add(temp);
        }

        public capitaine CAPITAINE
        {
            get
            {
                return captain;
            }
            set { this.captain = value; }
        }
        public string ID
        {
            get
            {
                return equipe_ID;
            }
            set { this.equipe_ID = value; }
        }

}
}